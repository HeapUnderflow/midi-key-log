use prettytable::{cell, format::TableFormat, row, Table};
use std::fs::File;
use std::io::Write;

mod midi;

static MIDI_CLIENT_NAME: &str = concat!(env!("CARGO_PKG_NAME"), "/v", env!("CARGO_PKG_VERSION"));

fn tb_format_spec() -> TableFormat {
	use prettytable::format::FormatBuilder;

	FormatBuilder::new().padding(1, 1).column_separator('|').build()
}

fn main() {
	println!(
		r#"Welcome to
 __  __ _     _ _ _
|  \/  (_) __| (_) |    ___   __ _
| |\/| | |/ _` | | |   / _ \ / _` |
| |  | | | (_| | | |__| (_) | (_| |
|_|  |_|_|\__,_|_|_____\___/ \__, |
                             |___/
"#
	);

	let midi_devs = find_midi_devs();

	if midi_devs.is_empty() {
		println!("No midi devices where found!");
		let _: Result<Option<String>, _> = promptly::prompt_opt("Press ENTER to exit...");
		return;
	}

	let mut midi_dev_tbl = Table::new();
	midi_dev_tbl.set_format(tb_format_spec());
	midi_dev_tbl.set_titles(row!["ID", "Name"]);
	for dev in &midi_devs {
		midi_dev_tbl.add_row(row![dev.0 + 1, &dev.1]);
	}
	midi_dev_tbl.printstd();

	let devid = loop {
		let mid: u8 = promptly::prompt("Choose a device to monitor (by ID)")
			.expect("Failed to retrieve id");
		if mid >= 1 && mid <= midi_devs.len() as u8 {
			break mid - 1;
		}

		println!("unknown id");
	};

	println!("Where should we write the log file?");
	let logf = promptly::prompt_default("", "midi.log".to_owned()).expect("fatal");

	let mut f = File::create(logf).unwrap();

	let mut ct = 0;
	connect_midi_idx(devid as usize, move |ts, bt, _| {
		ct += 1;
		print!("\rhandled {} events", ct);
		flush_stdout();

		write!(f, "{:>20} {}", ts, write_byte_sets(bt, 3)).expect("BT 1 F");

		let parse_note = match midi::MidiMessage::new(bt) {
			Ok(notes) => notes,
			Err(err) => {
				writeln!(f, "unknown midi message: {:?}", err).unwrap();
				return;
			}
		};

		match parse_note {
			midi::MidiMessage::Note(nt) => {
				write!(f, "Note -> ").expect("write fail");
				match nt.event {
					midi::MidiEvent::NoteOn => { write!(f, "On, ").unwrap() },
					midi::MidiEvent::NoteOff => { write!(f, "Off, ").unwrap() } 
				}

				writeln!(f, "Channel: {}, Note: {:?}, Velocity: {}", nt.channel, nt.note, nt.velocity).unwrap();
			}
			midi::MidiMessage::Controller(ct) => {
				writeln!(f, "Controller -> Name: {:?}, Value: {}", ct.controller, ct.value).expect("write fail");
			}
		}
	});

	loop {
		std::thread::sleep(std::time::Duration::from_secs(1));
	}
}

fn connect_midi_idx<F>(idx: usize, callback: F) where F: FnMut(u64, &[u8], &mut ()) + Send + 'static {
	let mut min = midir::MidiInput::new(MIDI_CLIENT_NAME).expect("Failed to create midi client");
	min.ignore(midir::Ignore::None);

	let ports = min.ports();
	let port = ports.iter().nth(idx).expect("Failed to find port").clone();
	min.connect(port, &format!("{}-{}", MIDI_CLIENT_NAME, "listen"), callback, ()).expect("Failed to connect");
}

fn find_midi_devs() -> Vec<(usize, String)> {
	let mut vec = Vec::new();

	let mut min = midir::MidiInput::new(MIDI_CLIENT_NAME).expect("Failed to create midi client");
	min.ignore(midir::Ignore::None);

	for (idx, port) in min.ports().iter().enumerate() {
		vec.push((
			idx,
			min.port_name(port).unwrap_or_else(|_| String::from("[ERROR: NO MIDID PORT NAME]")),
		))
	}

	vec
}

fn flush_stdout() {
	std::io::stdout().lock().flush().expect("Failed to flush stdout");
}

fn write_byte_sets(bts: &[u8], min: usize) -> String {
	if bts.is_empty() {
		return String::new();
	}

	let mut out = String::with_capacity(bts.len() * 3);
	let mut n = 0;
	for bt in bts {
		out.push_str(&format!("{:02X} ", bt));
		n += 1;
	}

	while n < min {
		out.push_str("   ");
		n += 1;
	}

	out
}
